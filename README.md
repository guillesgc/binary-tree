# Experis-BinaryTree

*Autor :* Guillermo García

*Version :* 1.0

## Tecnologias

*Java :* 1.8

*Spring boot* 

## Definición y diseño

Con el objetivo de realizar una solución optina para cumplir con los siguientes requerimientos: 

    1. Crear un árbol.
    2. Dado un arbol y dos nodos, retorne el ancestro común más cercano.
   
La solución fue diseñada para garantizar un bajo nivel de acoplamiento, 
alta escalabilidad y disponibilidad, teniendo presente los principios SOLID.


## Solución de requerimientos

*1. Se creó API rest que expone los siguientes métodos:

    */binaryTree/generate/tree/fromCollection 
     
    */binaryTree/findCommonAncestor/fromCollectionOfNodeDto     

    */binaryTree/findCommonAncestor/fromBinaryTreeDto
      
    */binaryTree/generate/collectionNote
     
*2.Tests :* El proyecto cuenta con las pruebas unitarias correspondientes del 
servicio BinaryTreeService, 
el cual contiene los métodos necesarios para satisfacer los requerimientos: 

	1. Crear un árbol.
	2. Dado un arbol y dos nodos, retorne el ancestro común más cercano.

## Instrucciones para despliegue

*1.* Descargar el código fuente desde *https://bitbucket.org/guillesgc/binary-tree/src*

    Clonar: git clone https://guillesgc@bitbucket.org/guillesgc/binary-tree.git

*2.* Desde consola ingresar a la carpeta del proyeco *binary-tree*

*3.* Ejecutar desde consola: mvn spring-boot:run

*4.* Desde postman u otra herramienta similiar (curl) ejecutar uno de los siguientes request: 

* http://localhost:9082/binaryTree/generate/collectionNote
Tipo: GET
Descripción: Esté método permite crear dinámicamente colecciones de números enteros que representarán los nodos de un árbol binario
 
        Ejemplo de response: 
        [
            [59,108,160,75],
            [17,194],[66],
            [94,46,121,167],
            [139,21,124,96,42,117],
            [100,59,136,120,123,75],
            [100,98],         
            [8],
            [130,97,115],
            [107],[117],
            [122,195,82],
            [146,180,196,167,92,14],
            [188]
        ]
        

* http://localhost:9082/binaryTree/findCommonAncestor/fromCollectionOfNodeDto
Tipo: POST y JSON
Descripción: Este método permite encontrar el ancestro común entre dos nodos a partir de unas colecciones de números enteros que representarán los nodos  que conformarán el árbol binario que se crea al invocar este método

    Request - Body
    
        {  
            "nodes":[
                [70,84,85],
                [70,84,78,80],
                [70,84,78,76],
                [70,49,54,51],
                [70,49,37,40],
                [70,49,37,22]
            ],
            "keyNode1":37,
            "keyNode2":51
        }

        Response - Body:
        {
            "key": 49,
            "level": 1,
            "left": null,
            "right": null
	    }


* http://localhost:9082/binaryTree/generate/tree/fromCollection
Tipo: POST y JSON
Descripción: Este método crea un árbol binario a partir de unas colecciones de números enteros que representarán los nodos

        Request - Body:
        [
            [70,84,85],
            [70,84,78,80],
            [70,84,78,76],
            [70,49,54,51],
            [70,49,37,40],
            [70,49,37,22]
        ]

        Response - Body:

        {
        "root": {
        "key": 70,
        "level": 0,
        "left": {
            "key": 49,
            "level": 1,
            "left": {
                "key": 37,
                "level": 2,
                "left": {
                    "key": 22,
                    "level": 3,
                    "left": null,
                    "right": null
                },
                "right": {
                    "key": 40,
                    "level": 3,
                    "left": null,
                    "right": null
                }
            },
            "right": {
                "key": 54,
                "level": 2,
                "left": {
                    "key": 51,
                    "level": 3,
                    "left": null,
                    "right": null
                },
                "right": null
            }
        },
        "right": {
            "key": 84,
            "level": 1,
            "left": {
                "key": 78,
                "level": 2,
                "left": {
                    "key": 76,
                    "level": 3,
                    "left": null,
                    "right": null
                },
                "right": {
                    "key": 80,
                    "level": 3,
                    "left": null,
                    "right": null
                }
            },
            "right": {
                "key": 85,
                "level": 2,
                "left": null,
                "right": null
            }
            }
           }
        }


* http://localhost:9082/binaryTree/findCommonAncestor/fromBinaryTreeDto
Tipo: POST y JSON
Descripción: Permite encontrar el ancestro común entre dos nodos de un árbol binario
        
        Request - Body:
        {  
        "binaryTree":{  
        "root":{  
         "key":70,
         "level":0,
         "left":{  
            "key":49,
            "level":1,
            "left":{  
               "key":37,
               "level":2,
               "left":{  
                  "key":22,
                  "level":3,
                  "left":null,
                  "right":null
               },
               "right":{  
                  "key":40,
                  "level":3,
                  "left":null,
                  "right":null
               }
            },
            "right":{  
               "key":54,
               "level":2,
               "left":{  
                  "key":51,
                  "level":3,
                  "left":null,
                  "right":null
               },
               "right":null
            }
         },
         "right":{  
            "key":84,
            "level":1,
            "left":{  
               "key":78,
               "level":2,
               "left":{  
                  "key":76,
                  "level":3,
                  "left":null,
                  "right":null
               },
               "right":{  
                  "key":80,
                  "level":3,
                  "left":null,
                  "right":null
               }
            },
            "right":{  
               "key":85,
               "level":2,
               "left":null,
               "right":null
            }
         }
        }
        },
        "keyNode1":76,
        "keyNode2":85
        }

        Response - Body:
        {
            "key": 84,
            "level": 1,
            "left": null,
            "right": null
        }
                
## Posibles Errores

*1.* Failed to execute goal org.apache.maven-plugins:maven-compiler-plugin:3.7.0.0:compile 
(default-compile) on project binary-tree: Faltal error compiling: invalid flag: -parameters.

Para solucionar el error anterior es necesario establecer la variable JAVA_HOME 
con la versión 1.8 de Java e.g *export JAVA_HOME="C:/Program Files/Java/jdk1.8.0_151"*

