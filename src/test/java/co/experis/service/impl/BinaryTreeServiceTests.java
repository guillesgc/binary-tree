package co.experis.service.impl;

import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import co.experis.MessageBean;
import co.experis.domain.dto.FindAncestorFromBinaryTreeDto;
import co.experis.domain.dto.FindAncestorFromCollectionOfNodeDto;
import co.experis.domain.entity.BinaryTree;
import co.experis.domain.entity.BinaryTreeNode;
import co.experis.service.BinaryTreeServiceContract;

/**
 * The Class BinaryTreeServiceTests.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
@RunWith(SpringRunner.class)
public class BinaryTreeServiceTests {

	/** The binary tree service. */
	private BinaryTreeServiceContract binaryTreeService;
	
	/** The message bean mock. */
	@Mock
	private MessageBean messageBeanMock;
	
	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		this.binaryTreeService = new  BinaryTreeService();
	}
	
	/**
	 * Generator collection node test.
	 * Test case to test dynamic generation of Collection of nodes to will create a new Binary tree
	 */
	@Test
	public void generatorCollectionNodeTest() {
		
		Collection<Collection<Integer>> collectionNode = binaryTreeService.generatorCollectionNode();
		Assert.assertNotNull(collectionNode);	
		Assert.assertTrue(collectionNode.size() > 0);		
	}
	
	/**
	 * Creates the binary tree from collection nodes test.
	 */
	@Test
	public void createBinaryTreeFromCollectionNodoTest() {		
		
		Collection<Collection<Integer>> collectionNode = binaryTreeService.generatorCollectionNode();
		Assert.assertNotNull(collectionNode);	
		Assert.assertTrue(collectionNode.size() > 0);
		
		BinaryTree bt = binaryTreeService.createBinaryTreeFromCollectionNode(collectionNode);
		Assert.assertNotNull(bt);
		Assert.assertNotNull(bt.getRoot());	
	}
	
	/**
	 * Find common ancestor from collection of nodes.
	 */
	@Test
	public void findCommonAncestorFromCollectionOfNodeTest() {		
		
		FindAncestorFromCollectionOfNodeDto findAncestor  = 
				new FindAncestorFromCollectionOfNodeDto ();
		
		findAncestor.setNodes(getCollectionsOfNodes());					
		findAncestor.setKeyNode1(40);
		findAncestor.setKeyNode2(78);
		
		BinaryTreeNode ancestor = binaryTreeService.findCommonAncestorFromCollectionOfNode(findAncestor);
		Assert.assertNotNull(ancestor);
		Assert.assertTrue(ancestor.getKey().equals(70)); //70
				
		findAncestor.setKeyNode1(51);
		findAncestor.setKeyNode2(37);
		ancestor = binaryTreeService.findCommonAncestorFromCollectionOfNode(findAncestor);
		Assert.assertNotNull(ancestor);
		Assert.assertTrue(ancestor.getKey().equals(49)); //49
		
		findAncestor.setKeyNode1(76);
		findAncestor.setKeyNode2(85);
		ancestor = binaryTreeService.findCommonAncestorFromCollectionOfNode(findAncestor);
		Assert.assertNotNull(ancestor);
		Assert.assertTrue(ancestor.getKey().equals(84)); //84		
	}
	
	/**
	 * Find common ancestor from binary tree.
	 */
	@Test
	public void findCommonAncestorFromBinaryTreeTest() {
		
		BinaryTree bt = binaryTreeService.createBinaryTreeFromCollectionNode(getCollectionsOfNodes());
		Assert.assertNotNull(bt);
		Assert.assertNotNull(bt.getRoot());
		
		FindAncestorFromBinaryTreeDto findAncestor = new FindAncestorFromBinaryTreeDto (bt, 40, 78);															
		BinaryTreeNode ancestor = binaryTreeService.findCommonAncestorFromBinaryTree(findAncestor);
		Assert.assertNotNull(ancestor);
		Assert.assertTrue(ancestor.getKey().equals(70)); //70
				
		findAncestor.setKeyNode1(51);
		findAncestor.setKeyNode2(37);
		ancestor = binaryTreeService.findCommonAncestorFromBinaryTree(findAncestor);
		Assert.assertNotNull(ancestor);
		Assert.assertTrue(ancestor.getKey().equals(49)); //49
		
		findAncestor.setKeyNode1(76);
		findAncestor.setKeyNode2(85);
		ancestor = binaryTreeService.findCommonAncestorFromBinaryTree(findAncestor);
		Assert.assertNotNull(ancestor);
		Assert.assertTrue(ancestor.getKey().equals(84)); //84			
	}
	
	/**
	 * Not exists node on binary tree.
	 * This test case test that no
	 */
	@Test
	public void notExistsNodeOnBinaryTreeTest() {
		try {
		setFieldValueToSuperClass(binaryTreeService, "message", messageBeanMock,0);
		Mockito.when((messageBeanMock).getNodeNotExist()).thenReturn("The node with key [%s] not exist in the tree");		
		BinaryTree bt = binaryTreeService.createBinaryTreeFromCollectionNode(getCollectionsOfNodes());
		Assert.assertNotNull(bt);
		Assert.assertNotNull(bt.getRoot());
			try {
				binaryTreeService.existsNodeOnBinaryTree(400, bt);
				Assert.fail("Error, a exepcion IllegalArgumentException");
			}catch(Exception e) {
				Assert.assertTrue(e instanceof IllegalArgumentException);
			}
		} catch (Exception e1) {
			fail("Failure test case  existsNodeOnBinaryTreeTest : " + e1.getMessage());
		}
	}

	
	/**
	 * Gets the collections of nodes.
	 *
	 * @return the collections of nodes
	 */
	private Collection<Collection<Integer>> getCollectionsOfNodes() {
				
		Collection<Collection<Integer>> nodes = new ArrayList<Collection<Integer>>();
		Collection<Integer> lista = new ArrayList<>(); 
		//70,84,85
		lista.add(70);
		lista.add(84);
		lista.add(85);		
		nodes.add(lista);
		
		//70,84,78,80
		lista = new ArrayList<>();
		lista.add(70);
		lista.add(84);
		lista.add(78);		
		lista.add(80);
		nodes.add(lista);	
		
		//70,84,78,76
		lista = new ArrayList<>();
		lista.add(70);
		lista.add(84);
		lista.add(78);		
		lista.add(76);
		nodes.add(lista);	
		
		//70,49,54,51
		lista = new ArrayList<>();
		lista.add(70);
		lista.add(49);
		lista.add(54);		
		lista.add(51);
		nodes.add(lista);
		
		//70,49,37,40		
		lista = new ArrayList<>();
		lista.add(70);
		lista.add(49);
		lista.add(37);		
		lista.add(40);
		nodes.add(lista);

		//70,49,37,22		
		lista = new ArrayList<>();
		lista.add(70);
		lista.add(49);
		lista.add(37);		
		lista.add(22);
		nodes.add(lista);
		
			
		return nodes;	
	}
	
	/**
	 * Sets the field value to super class.
	 *
	 * @param cmp the cmp
	 * @param fieldName the field name
	 * @param mock the mock
	 * @param level the level
	 * @throws SecurityException the security exception
	 * @throws NoSuchFieldException the no such field exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	private static void setFieldValueToSuperClass(Object cmp, String fieldName, Object mock, int level)
			throws SecurityException,
			NoSuchFieldException,
			IllegalArgumentException,
			IllegalAccessException {
				
		Class<?> c = cmp.getClass();
		for(int i = 0; i < level; i++) {
			c = c.getSuperclass();
		}
		Field field = c.getDeclaredField(fieldName);
		field.setAccessible(true);
		field.set(cmp, mock);			
	}

}
