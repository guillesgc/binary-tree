package co.experis.domain.entity;

/**
 * The Class BinaryTreeNodo.
 * This class is used to represent the node of the Binary Tree.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
public class BinaryTreeNode {

	/** The key. */
	private Integer key;
	
	/** The level. */
	private Integer level;	
	
	/** The left. */
	private BinaryTreeNode left;  
	
	/** The right. */
	private BinaryTreeNode right;
	
	/**
	 * Instantiates a new binary tree nodo.
	 */
	public BinaryTreeNode() {
		
	}
	
	/**
	 * Instantiates a new binary tree nodo.
	 *
	 * @param key the key
	 * @param level the level
	 */
	public 	BinaryTreeNode(Integer key, Integer level) {
		this.key = key;
		this.level = level;
	}
	
	/**
	 * Instantiates a new binary tree nodo.
	 *
	 * @param key the key
	 */
	public 	BinaryTreeNode(int key) {
		this.key = key;
	}
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public Integer getKey() {
		return key;
	}
	
	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(int key) {
		this.key = key;
	}
	
	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}
	
	/**
	 * Sets the level.
	 *
	 * @param level the new level
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/**
	 * Gets the left.
	 *
	 * @return the left
	 */
	public BinaryTreeNode getLeft() {
		return left;
	}
	
	/**
	 * Sets the left.
	 *
	 * @param left the new left
	 */
	public void setLeft(BinaryTreeNode left) {
		this.left = left;
	}
	
	/**
	 * Gets the right.
	 *
	 * @return the right
	 */
	public BinaryTreeNode getRight() {
		return right;
	}
	
	/**
	 * Sets the right.
	 *
	 * @param right the new right
	 */
	public void setRight(BinaryTreeNode right) {
		this.right = right;
	} 
  
	
}