package co.experis.domain.entity;

/**
 * This class is used to represent the Binary Tree.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
public class BinaryTree {

	/** The root. */
	private BinaryTreeNode root;

	/**
	 * Gets the root.
	 *
	 * @return the root
	 */
	public BinaryTreeNode getRoot() {
		return root;
	}

	/**
	 * Sets the root.
	 *
	 * @param root the new root
	 */
	public void setRoot(BinaryTreeNode root) {
		this.root = root;
	}	
}
