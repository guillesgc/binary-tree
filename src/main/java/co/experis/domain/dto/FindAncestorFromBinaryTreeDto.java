package co.experis.domain.dto;

import javax.validation.constraints.NotNull;

import co.experis.domain.entity.BinaryTree;

/**
 * This class is used to find a common ancestor between two nodes in the binary tree.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
public class FindAncestorFromBinaryTreeDto {
	
	/** The binary tree. */
	@NotNull
	private BinaryTree binaryTree;
	
	/** The key node 1. */
	@NotNull
	private Integer keyNode1;
	
	/** The key node 2. */
	@NotNull
	private Integer keyNode2;
	
	/**
	 * Instantiates a new find ancestor from binary tree dto.
	 */
	public FindAncestorFromBinaryTreeDto() {
		
	}
				
	/**
	 * Instantiates a new find ancestor from binary tree dto.
	 *
	 * @param binaryTree the binary tree
	 * @param keyNode1 the key node 1
	 * @param keyNode2 the key node 2
	 */
	public FindAncestorFromBinaryTreeDto(@NotNull BinaryTree binaryTree, @NotNull Integer keyNode1,
			@NotNull Integer keyNode2) {
		super();
		this.binaryTree = binaryTree;
		this.keyNode1 = keyNode1;
		this.keyNode2 = keyNode2;
	}


	/**
	 * Gets the binary tree.
	 *
	 * @return the binary tree
	 */
	public BinaryTree getBinaryTree() {
		return binaryTree;
	}

	/**
	 * Sets the binary tree.
	 *
	 * @param binaryTree the new binary tree
	 */
	public void setBinatyTree(BinaryTree binaryTree) {
		this.binaryTree = binaryTree;
	}
	
	/**
	 * Gets the key node 1.
	 *
	 * @return the key node 1
	 */
	public Integer getKeyNode1() {
		return keyNode1;
	}

	/**
	 * Sets the key node 1.
	 *
	 * @param keyNode1 the new key node 1
	 */
	public void setKeyNode1(Integer keyNode1) {
		this.keyNode1 = keyNode1;
	}

	/**
	 * Gets the key node 2.
	 *
	 * @return the key node 2
	 */
	public Integer getKeyNode2() {
		return keyNode2;
	}

	/**
	 * Sets the key node 2.
	 *
	 * @param keyNode2 the new key node 2
	 */
	public void setKeyNode2(Integer keyNode2) {
		this.keyNode2 = keyNode2;
	}
	
}
