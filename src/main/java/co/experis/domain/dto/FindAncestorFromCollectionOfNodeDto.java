package co.experis.domain.dto;

import java.util.Collection;

import javax.validation.constraints.NotNull;

/**
 * This class is used to find a common ancestor between two nodes in the binary tree.
 * 
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
public class FindAncestorFromCollectionOfNodeDto {
	
	/** Collection of integer to represent the key of nodes. */
	@NotNull
	private Collection<Collection<Integer>> nodes;
	
	/** The key node 1. */
	@NotNull
	private Integer keyNode1;
	
	/** The key node 2. */
	@NotNull
	private Integer keyNode2;
	
	/**
	 * Instantiates a new find ancestor from collection of node dto.
	 */
	public FindAncestorFromCollectionOfNodeDto() {
		
	}

	/**
	 * Gets the nodes.
	 *
	 * @return the nodes
	 */
	public Collection<Collection<Integer>> getNodes() {
		return nodes;
	}

	/**
	 * Sets the nodes.
	 *
	 * @param nodes the new nodes
	 */
	public void setNodes(Collection<Collection<Integer>> nodes) {
		this.nodes = nodes;
	}

	/**
	 * Gets the key node 1.
	 *
	 * @return the key node 1
	 */
	public Integer getKeyNode1() {
		return keyNode1;
	}

	/**
	 * Sets the key node 1.
	 *
	 * @param keyNode1 the new key node 1
	 */
	public void setKeyNode1(Integer keyNode1) {
		this.keyNode1 = keyNode1;
	}

	/**
	 * Gets the key node 2.
	 *
	 * @return the key node 2
	 */
	public Integer getKeyNode2() {
		return keyNode2;
	}

	/**
	 * Sets the key node 2.
	 *
	 * @param keyNode2 the new key node 2
	 */
	public void setKeyNode2(Integer keyNode2) {
		this.keyNode2 = keyNode2;
	}

	
}
