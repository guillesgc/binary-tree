/*
 * Copyright 2019. Boozni SAS
 */
package co.experis.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Random;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.experis.MessageBean;
import co.experis.domain.dto.FindAncestorFromBinaryTreeDto;
import co.experis.domain.dto.FindAncestorFromCollectionOfNodeDto;
import co.experis.domain.entity.BinaryTree;
import co.experis.domain.entity.BinaryTreeNode;
import co.experis.service.BinaryTreeServiceContract;

/**
 * The Class BinaryTreeService, is used to apply logical about functions and business to find and create binary tree.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
@Service
public class BinaryTreeService implements BinaryTreeServiceContract {

	/** The message. */
	@Autowired
	private MessageBean message; 
		
	/** The num of collections. */
	@Value("${params.num-of-collections:10}")
	private Integer numOfCollections;
	
	/** The max element of collections. */
	@Value("${params.max-element-of-collections:10}")
	private Integer maxElementOfCollections;
	
	/** The max value. */
	@Value("${params.max-value:80}")
	private Integer maxValue;
			
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BinaryTree createBinaryTreeFromCollectionNode(Collection<Collection<Integer>> nodes) {
		BinaryTree bt = new BinaryTree();

		for (Collection<Integer> integerList : nodes) {
			for (Integer key : integerList) {
				if (Objects.isNull(bt.getRoot())) {
					BinaryTreeNode root = new BinaryTreeNode(key);
					root.setLevel(NumberUtils.INTEGER_ZERO);
					bt.setRoot(root);
				} else {
					addNodoToBinaryTree(new BinaryTreeNode(key), bt.getRoot());
				}
			}
		}
		return bt;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BinaryTreeNode leastCommonAncestor(final BinaryTreeNode currentNode, final Integer keyNode1,
			final Integer keyNode2) {

		while (Objects.nonNull(currentNode)) {
			if (keyNode1 < currentNode.getKey() && keyNode2 < currentNode.getKey())
				return leastCommonAncestor(currentNode.getLeft(), keyNode1, keyNode2);
			else if (keyNode1 > currentNode.getKey() && keyNode2 > currentNode.getKey())
				return leastCommonAncestor(currentNode.getRight(), keyNode1, keyNode2);
			else
				return currentNode;
		}

		throw new IllegalArgumentException(
				String.format(message.getNodeDontHaveAncestor(), keyNode1, keyNode2));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BinaryTreeNode findCommonAncestorFromCollectionOfNode(final FindAncestorFromCollectionOfNodeDto findCommonAncestorDto)
			 {
		
		BinaryTree bt = createBinaryTreeFromCollectionNode(findCommonAncestorDto.getNodes());

		existsNodeOnBinaryTree(findCommonAncestorDto.getKeyNode1(), bt);
		existsNodeOnBinaryTree(findCommonAncestorDto.getKeyNode2(), bt);
		FindAncestorFromBinaryTreeDto findAncestorFromBinaryTreeDto = new FindAncestorFromBinaryTreeDto(bt,
				findCommonAncestorDto.getKeyNode1(), findCommonAncestorDto.getKeyNode2());
					
		return findCommonAncestorFromBinaryTree (findAncestorFromBinaryTreeDto);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BinaryTreeNode findCommonAncestorFromBinaryTree(final FindAncestorFromBinaryTreeDto findAncestorFromBinaryTreeDto)
			 {

		existsNodeOnBinaryTree(findAncestorFromBinaryTreeDto.getKeyNode1(),
				findAncestorFromBinaryTreeDto.getBinaryTree());
		existsNodeOnBinaryTree(findAncestorFromBinaryTreeDto.getKeyNode2(),
				findAncestorFromBinaryTreeDto.getBinaryTree());

		BinaryTreeNode commonAncestor = leastCommonAncestor(findAncestorFromBinaryTreeDto.getBinaryTree().getRoot(),
				findAncestorFromBinaryTreeDto.getKeyNode1(), findAncestorFromBinaryTreeDto.getKeyNode2());
					
		return new BinaryTreeNode(commonAncestor.getKey(), commonAncestor.getLevel());		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean existsNodeOnBinaryTree(final Integer key, final BinaryTree binaryTree)  {

		BinaryTreeNode findNode = binaryTree.getRoot();
		while (findNode != null) {
			if (key.equals(findNode.getKey())) {
				return true;
			} else if (key > findNode.getKey()) {
				findNode = findNode.getRight();
			} else {
				findNode = findNode.getLeft();
			}
		}
		throw new IllegalArgumentException(String.format(message.getNodeNotExist(), key));
	}

	/**
	 * Adds the new nodo to binary tree.
	 *
	 * @param newNode the new node
	 * @param parentNode the parent node
	 */
	private void addNodoToBinaryTree(final BinaryTreeNode newNode, final BinaryTreeNode parentNode) {
		addNodoToBinaryTree(newNode, parentNode, parentNode);
	}

	/**
	 * Adds the new nodo to binary tree.
	 *
	 * @param newNode the new node
	 * @param parentNode the parent node
	 * @param root the root
	 */
	private void addNodoToBinaryTree(final BinaryTreeNode newNode, BinaryTreeNode parentNode, BinaryTreeNode root) {
		if (Objects.isNull(root)) {
			newNode.setLevel(NumberUtils.INTEGER_ZERO);
			parentNode = newNode;
		} else {
			if (Objects.nonNull(parentNode)) {
				conditionToAdd(newNode, parentNode, root);
			} else {
				throw new IllegalArgumentException("The parent node can't be null");
			}
		}
	}

	/**
	 * Apply condition and logical to need to add a new node to the binary tree.
	 *
	 * @param newNode the new node
	 * @param parentNode the parent node
	 * @param root the root
	 */
	private void conditionToAdd(final BinaryTreeNode newNode, final BinaryTreeNode parentNode,
			final BinaryTreeNode root) {

		if (newNode.getKey() < parentNode.getKey()) {
			if (Objects.isNull(parentNode.getLeft())) {
				newNode.setLevel(parentNode.getLevel() + NumberUtils.INTEGER_ONE);
				parentNode.setLeft(newNode);
			} else {
				addNodoToBinaryTree(newNode, parentNode.getLeft(), root);
			}
		} else if (newNode.getKey() > parentNode.getKey()) {

			if (Objects.isNull(parentNode.getRight())) {
				newNode.setLevel(parentNode.getLevel() + NumberUtils.INTEGER_ONE);
				parentNode.setRight(newNode);
			} else {
				addNodoToBinaryTree(newNode, parentNode.getRight(), root);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Collection<Integer>> generatorCollectionNode() {
		
		int numberOfColectionOfNode = generatorOfNumbers(numOfCollections);
		
		Collection<Collection<Integer>> nodes = new ArrayList<Collection<Integer>>();
		for (int i = 0; i<numberOfColectionOfNode ; i++) {
			
			int numberOfNode = generatorOfNumbers(maxElementOfCollections);
			Collection<Integer> list = new ArrayList<>();
			for(int j = 0; j < numberOfNode; j++) {
				
				list.add(generatorOfNumbers(maxValue));
			}
			
			if(!list.isEmpty()) {
				nodes.add(list);
			}
		}
		
		return nodes;
	}
	
	
	/**
	 * Generator of numbers.
	 *
	 * @param max the max
	 * @return the int
	 */
	private int generatorOfNumbers(int max) {
		return generatorOfNumbers(max, NumberUtils.INTEGER_ZERO);
	}
	
	/**
	 * Generator of numbers.
	 *
	 * @param max the max
	 * @param min the min
	 * @return the int
	 */
	private int generatorOfNumbers(int max, int min) {	
		Random r = new Random();
	    return r.nextInt((max - min) + 1) + min;
	    
	}
	
}
