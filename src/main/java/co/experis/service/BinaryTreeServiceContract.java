package co.experis.service;

import java.util.Collection;

import co.experis.domain.dto.FindAncestorFromBinaryTreeDto;
import co.experis.domain.dto.FindAncestorFromCollectionOfNodeDto;
import co.experis.domain.entity.BinaryTree;
import co.experis.domain.entity.BinaryTreeNode;

/**
 * The Interface BinaryTreeServiceContract.
 * Definition of the methods need to manage the binary tree
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
public interface BinaryTreeServiceContract {

	/**
	 * Creates the binary tree from collection node.
	 *
	 * @param nodes the nodes
	 * @return the binary tree
	 */
	public BinaryTree createBinaryTreeFromCollectionNode(final Collection<Collection<Integer>> nodes);

	/**
	 * Exists node on binary tree.
	 *
	 * @param key the key
	 * @param bt the bt
	 * @return true, if successful
	 */
	public boolean existsNodeOnBinaryTree(final Integer key, final BinaryTree bt) ;

	/**
	 * Find common ancestor from collection of node.
	 *
	 * @param findAncestorFromCollectionOfNodeDto the find ancestor from collection of node dto
	 * @return the binary tree node 
	 */
	public BinaryTreeNode findCommonAncestorFromCollectionOfNode(
			final FindAncestorFromCollectionOfNodeDto findAncestorFromCollectionOfNodeDto);

	/**
	 * Find common ancestor from binary tree.
	 *
	 * @param findAncestorFromBinaryTreeDto the find ancestor from binary tree dto
	 * @return the binary tree node 
	 */
	public BinaryTreeNode findCommonAncestorFromBinaryTree(
			final FindAncestorFromBinaryTreeDto findAncestorFromBinaryTreeDto);

	/**
	 * Least common ancestor.
	 *
	 * @param currentNode the current node
	 * @param keyNode1 the key node 1
	 * @param keyNode2 the key node 2
	 * @return the binary tree node
	 */
	public BinaryTreeNode leastCommonAncestor(final BinaryTreeNode currentNode, final Integer keyNode1,
			final Integer keyNode2);

	/**
	 * Generator collection nodo.
	 *
	 * @return the collection
	 */
	public Collection<Collection<Integer>> generatorCollectionNode();

}
