package co.experis.controller.impl;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.experis.controller.BinaryTreeControllerContract;
import co.experis.domain.dto.FindAncestorFromBinaryTreeDto;
import co.experis.domain.dto.FindAncestorFromCollectionOfNodeDto;
import co.experis.domain.entity.BinaryTree;
import co.experis.domain.entity.BinaryTreeNode;
import co.experis.service.BinaryTreeServiceContract;

/**
 * The Class BinaryTreeController.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
@RestController
@RequestMapping("/binaryTree")
public class BinaryTreeController implements BinaryTreeControllerContract{
	
	/** The binary tree service. */
	@Autowired
	private BinaryTreeServiceContract binaryTreeService;

	/**
	 * Creates the binary tree from collection nodes.
	 *
	 * @param nodes the nodes
	 * @return the response entity
	 */
	@Override
	@PostMapping("/generate/tree/fromCollection")
	public ResponseEntity<BinaryTree> createBinaryTreeFromCollectionNode(
			@Valid @RequestBody final Collection<Collection<Integer>> nodes) {

		return new ResponseEntity<>(binaryTreeService.createBinaryTreeFromCollectionNode(nodes), HttpStatus.OK);
	}

	/**
	 * Find common ancestor.
	 *
	 * @param findCommonAncestorDto the find common ancestor dto
	 * @return the response entity
	 */
	@Override
	@PostMapping("/findCommonAncestor/fromCollectionOfNodeDto")
	public ResponseEntity<BinaryTreeNode> findCommonAncestor(
			@Valid @RequestBody FindAncestorFromCollectionOfNodeDto findCommonAncestorDto) {

		return new ResponseEntity<>(binaryTreeService.findCommonAncestorFromCollectionOfNode(findCommonAncestorDto),
				HttpStatus.OK);
	}

	/**
	 * Find common ancestor.
	 *
	 * @param findAncestorFromBinaryTreeDto the find ancestor from binary tree dto
	 * @return the response entity
	 */
	@Override
	@PostMapping("/findCommonAncestor/fromBinaryTreeDto")
	public ResponseEntity<BinaryTreeNode> findCommonAncestor(
			@Valid @RequestBody FindAncestorFromBinaryTreeDto findAncestorFromBinaryTreeDto){

		return new ResponseEntity<>(binaryTreeService.findCommonAncestorFromBinaryTree(findAncestorFromBinaryTreeDto),
				HttpStatus.OK);
	}

	/**
	 * Generator collection node.
	 *
	 * @return the response entity
	 */
	@Override
	@GetMapping("/generate/collectionNote")	
	public ResponseEntity<Collection<Collection<Integer>>> generatorCollectionNode() {
		return new ResponseEntity<>(binaryTreeService.generatorCollectionNode(), HttpStatus.OK);
	}	
	
}
