package co.experis.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import co.experis.domain.dto.FindAncestorFromBinaryTreeDto;
import co.experis.domain.dto.FindAncestorFromCollectionOfNodeDto;
import co.experis.domain.entity.BinaryTree;
import co.experis.domain.entity.BinaryTreeNode;

/**
 * The Interface BinaryTreeControllerContract.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
public interface BinaryTreeControllerContract {

	/**
	 * Creates the binary tree from collection node.
	 *
	 * @param nodes the nodes
	 * @return the response entity
	 */
	public ResponseEntity<BinaryTree> createBinaryTreeFromCollectionNode(
			@Valid @RequestBody final Collection<Collection<Integer>> nodes);

	/**
	 * Find common ancestor.
	 *
	 * @param findCommonAncestorDto the find common ancestor dto
	 * @return the response entity
	 */
	public ResponseEntity<BinaryTreeNode> findCommonAncestor(
			@Valid @RequestBody FindAncestorFromCollectionOfNodeDto findCommonAncestorDto);

	/**
	 * Find common ancestor.
	 *
	 * @param findAncestorFromBinaryTreeDto the find ancestor from binary tree dto
	 * @return the response entity
	 */
	@PostMapping("/findCommonAncestor/fromBinaryTreeDto")
	public ResponseEntity<BinaryTreeNode> findCommonAncestor(
			@Valid @RequestBody FindAncestorFromBinaryTreeDto findAncestorFromBinaryTreeDto);

	/**
	 * Generator collection node.
	 *
	 * @return the response entity
	 */
	public ResponseEntity<Collection<Collection<Integer>>> generatorCollectionNode();

}
