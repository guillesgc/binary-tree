package co.experis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The Class MessageBean.
 *
 * @author <a href="guillermo.garcia.carrasquilla@gmail.com"> Guillermo García</a>
 * @version 1.0
 */
@Component
public class MessageBean {
	
	/** The node dont have ancestor. */
	@Value("${message.node.dont-have-ancestor:false}")
	private String nodeDontHaveAncestor;
	
	/** The node not exist. */
	@Value("${message.node.node-not-exist:false}")
	private String nodeNotExist;

	/**
	 * Gets the node dont have ancestor.
	 *
	 * @return the node dont have ancestor
	 */
	public String getNodeDontHaveAncestor() {
		return nodeDontHaveAncestor;
	}

	/**
	 * Sets the node dont have ancestor.
	 *
	 * @param nodeDontHaveAncestor the new node dont have ancestor
	 */
	public void setNodeDontHaveAncestor(String nodeDontHaveAncestor) {
		this.nodeDontHaveAncestor = nodeDontHaveAncestor;
	}

	/**
	 * Gets the node not exist.
	 *
	 * @return the node not exist
	 */
	public String getNodeNotExist() {
		return nodeNotExist;
	}

	/**
	 * Sets the node not exist.
	 *
	 * @param nodeNotExist the new node not exist
	 */
	public void setNodeNotExist(String nodeNotExist) {
		this.nodeNotExist = nodeNotExist;
	}
	
	
}
